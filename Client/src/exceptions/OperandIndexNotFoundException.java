package exceptions;

/**
 * The exception Operand index not found exception.
 */
public class OperandIndexNotFoundException extends Exception{

  /**
   * Instantiates a new Operand index not found exception.
   *
   * @param index the index
   */
  public OperandIndexNotFoundException(int index){
    super("No operator found with index "+index);
  }
}
