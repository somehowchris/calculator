package interfaces;

import models.enums.InputOperation;

/**
 * The interface to process input operation.
 */
public interface ProcessInputOperation {
  /**
   * Process input operation.
   *
   * @param inputOperation the input operation
   */
  void processInputOperation(InputOperation inputOperation);
}
