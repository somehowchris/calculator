package interfaces;

/**
 * The interface to process char callback.
 */
public interface ProcessCharCallback {
  /**
   * Process char.
   *
   * @param input the input
   * @throws Exception the exception
   */
  void processChar(char input) throws Exception;
}
