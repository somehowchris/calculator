package interfaces;

import models.enums.MathOperation;

/**
 * The interface to process math operation callback.
 */
public interface ProcessMathOperationCallback {
  /**
   * Process math operation.
   *
   * @param mathOperation the math operation
   */
  void processMathOperation(MathOperation mathOperation);
}
