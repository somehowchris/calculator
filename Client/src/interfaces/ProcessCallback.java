package interfaces;

/**
 * The interface to process callback.
 */
public interface ProcessCallback extends ProcessCharCallback, ProcessInputOperation, ProcessMathOperationCallback {
}
