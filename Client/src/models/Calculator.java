package models;

import models.enums.MathOperation;

/**
 * The type Calculator
 */
public class Calculator {

  private MathOperation mathOperation;
  private Double operand1;
  private Double operand2;
  private Double result;

  /**
   * Set math operation.
   *
   * @param op The operation
   */
  public void setMathOperation(MathOperation op){
    if (this.operand1 != null && this.operand2 != null) this.calculateToAddMoreNumbers();
    this.mathOperation = op;
  }

  /**
   * Add operand and if there are already two calculate the previous.
   *
   * @param operand The operand
   */
  public void addOperand(double operand){
    if (this.operand1 != null && this.operand2 != null) this.calculateToAddMoreNumbers();
    if (this.operand1 == null) {
      this.operand1 = Double.valueOf(operand);
      return;
    }
    this.operand2 = Double.valueOf(operand);
  }

  /**
   * Get the result
   *
   * @return the result
   */
  public double getResult(){
    return this.result;
  }

  /**
   * Calculate function
   *
   * @return the result
   * @throws Exception Exception if there's one pre calculating
   */
  public double calculate() throws Exception {
    if(this.mathOperation == null){
      throw new Exception("Cant find current operator");
    }
    if(this.operand1 == null || this.operand2 == null){
      throw new Exception("Operand missing");
    }
    this.result = this.mathOperation.calculate(this.operand1, this.operand2);
    this.operand1 = null;
    this.operand2 = null;
    return this.result;
  }


  /**
   * Reset calculation.
   */
  public void resetCalculation(){
    this.operand1 = null;
    this.operand2 = null;
    this.result = null;
    this.mathOperation = null;
  }


  private void calculateToAddMoreNumbers(){
    if(this.operand1 != null && this.operand2 != null){
      try {
        this.operand1 = this.calculate();
        this.operand2 = null;
      } catch (Exception e) {
      }
    }
  }
}
