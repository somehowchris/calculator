package models.enums;

/**
 * The enum for Math operations.
 */
public enum MathOperation {
  MULTIPLY("*"),
  DIVIDE("/"),
  ADD("+"),
  SUBTRACT("-"),
  POWEROF("^");

  private String sign;

  MathOperation(String form ){
    this.sign = form;
  }

  /**
   * Get the sign of the operation
   *
   * @return the sign
   */
  public String getSign(){return this.sign;};

  /**
   * Calculate with two doubles and the current math operation
   *
   * @param i1 number #1
   * @param i2 number #2
   * @return the result
   */
  public double calculate(double i1, double i2){
    switch (this){
      case MULTIPLY:
        return i1*i2;
      case ADD:
        return i1+i2;
      case DIVIDE:
        return i1/i2;
      case SUBTRACT:
        return i1-i2;
      case POWEROF:
        return Math.pow(i1, i2);
      default:
        return 0.0;
    }
  }
}
