package models.enums;

/**
 * The enum for  Input operations.
 */
public enum InputOperation {
  CLEAR_ENTRY("CE"),
  CLEAR_ALL("CA"),
  EQUALS("="),
  REVERSE_SIGN("+/-");

  /**
   * The Sign.
   */
  String sign;

  InputOperation(String sign){
    this.sign = sign;
  }

  /**
   * Get sign of the operation
   *
   * @return the sign
   */
  public String getSign(){
    return this.sign;
  }
}
