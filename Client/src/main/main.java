package main;

import views.CalculatorView;

/**
 * The type Main.
 */
public class main {
  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    new CalculatorView();
  }
}
