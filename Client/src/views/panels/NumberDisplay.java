package views.panels;

import interfaces.ProcessCharCallback;

import javax.swing.*;
import java.awt.*;

/**
 * Displaying the current output.
 */
public class NumberDisplay extends JLabel implements ProcessCharCallback {

  /**
   * Instantiates a new Number display.
   */
  public NumberDisplay(){
    this.setForeground(Color.green);
    this.setBackground(Color.gray);
    this.setOpaque(true);
    this.setFont(new Font(this.getFont().getName(), Font.BOLD,32));
    this.setText("0");
    this.setHorizontalAlignment(SwingConstants.RIGHT);
  }

  /**
   * Set the given output
   *
   * @param output String to output
   */
  public void setOutput(String output){
    try {
      this.setText(String.valueOf(Integer.valueOf(output.substring(0,output.length() > 14 ? 14 : output.length()))));
    } catch (Exception e) {
      this.setText(String.valueOf(Double.valueOf(output.substring(0,output.length() > 14 ? 14 : output.length()))));
    }
  }


  /**
   * Clears the output.
   */
  public void clearOutput(){
    setOutput("0");
  }

  /**
   * Inverts the sign of the displayed number
   */
  public void switchSign(){
    try {
      this.setText(String.valueOf(Integer.valueOf(this.getText())*-1));
    } catch (Exception e) {
      this.setText(String.valueOf(Double.valueOf(this.getText())*-1));
    }
  }

  /**
   * Get the current displayed output.
   * @return the string
   */
  public String getOutput(){
    return this.getText();
  }

  @Override
  public void processChar(char input) throws Exception {
    if(this.getText().length() >= 14){
      throw new Exception("Too long input");
    }

    try {
      this.setText(String.valueOf(Integer.valueOf(this.getText()+String.valueOf(input))));
    } catch (Exception e) {
      this.setText(String.valueOf(Double.valueOf(this.getText()+String.valueOf(input))));
    }
  }
}
