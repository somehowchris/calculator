package views.buttons;

import interfaces.ProcessCharCallback;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The type Digit button.
 */
public class DigitButton extends BasicButton {
  /**
   * Instantiates a new Digit button with a listener to callback a given operation callback.
   *
   * @param digit  the digit
   * @param parent the parent
   */
  public DigitButton(final char digit, final ProcessCharCallback parent){
      this.setText(String.valueOf(digit));

      this.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          try {
            parent.processChar(digit);
          } catch (Exception ex) {
          }
        }
      });
    }
}
