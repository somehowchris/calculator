package views.buttons;

import interfaces.ProcessMathOperationCallback;
import models.enums.MathOperation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The type Math operation button.
 */
public class MathOperationButton extends BasicButton {
  /**
   * Instantiates a new Math operation button with a listener to callback a given operation callback.
   *
   * @param mathOperation the math operation
   * @param parent        the parent
   */
  public MathOperationButton(final MathOperation mathOperation, final ProcessMathOperationCallback parent){
    this.setText(mathOperation.getSign());

    this.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.processMathOperation(mathOperation);
      }
    });
  }
}
