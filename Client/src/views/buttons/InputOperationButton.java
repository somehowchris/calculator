package views.buttons;

import interfaces.ProcessInputOperation;
import models.enums.InputOperation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The type Input operation button.
 */
public class InputOperationButton extends BasicButton {

  /**
   * Instantiates a new Input operation button with a listener to callback a given operation callback.
   *
   * @param inputOperation the input operation
   * @param parent         the parent
   */
  public InputOperationButton(final InputOperation inputOperation, final ProcessInputOperation parent){
    this.setText(inputOperation.getSign());

    this.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        parent.processInputOperation(inputOperation);
      }
    });
  }
}
