package views.buttons;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * The type Basic button.
 */
public class BasicButton extends JButton {

  /**
   * Instantiates a new Basic button with its basic design.
   */
  BasicButton(){
    Border emptyBorder = BorderFactory.createRaisedSoftBevelBorder();
    this.setBorder(emptyBorder);
    this.setSize(80,80);
    this.setFont(new Font(this.getFont().getName(), this.getFont().getStyle(), 24));
  }
}
