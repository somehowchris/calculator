package views.helpers;

import javax.swing.*;

/**
 * The Body handler
 */
public class Body {

  private int height = 480;
  private int width = 480;
  private JFrame frame;

  /**
   * Set visibility of the frame
   *
   * @param visible the visible
   */
  public void setVisibility(boolean visible){
    frame.setVisible(visible);
  }

  /**
   * Instantiates a new Body.
   *
   * @param frame the frame
   */
  public Body(JFrame frame){
    this.frame = frame;
    frame.setSize(this.width, this.height);
    frame.setResizable(false);
  }
}
